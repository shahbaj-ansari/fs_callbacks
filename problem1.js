/*
    Problem 1:
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/

const fs=require('fs');
const path=require('path')

function makeDirWithJSONFilesAndDeleteThoseFiles(dirName,callback){
    
    if( typeof(dirName)!='string' ||
        typeof(callback)!='function'){
        
        throw new Error('Enter all arguments in order : dirName: string, callback: function');
    }

    fs.mkdir(path.join(__dirname,dirName),err=>{
        if(err){
            throw err;
        }
        callback(`${dirName} : Directory Created`);
    
        for(let i=0;i<3;i++){
            fs.writeFile(path.join(__dirname,dirName,`${String.fromCharCode(65+i)}.json`),`{"fileName":"${String.fromCharCode(65+i)}"}`,err=>{
                if(err){
                    throw err;
                }
                callback(`${String.fromCharCode(65+i)}.json : File Created`);
            })
        }

        setTimeout(()=>{
            for(let i=0;i<3;i++){
                fs.unlink(path.join(__dirname,dirName,`${String.fromCharCode(65+i)}.json`),err=>{
                    if(err){
                        throw err;
                    }
                    callback(`${String.fromCharCode(65+i)}.json : File Deleted`);
                });  
            }    
        },1500);
    })
}

module.exports=makeDirWithJSONFilesAndDeleteThoseFiles;
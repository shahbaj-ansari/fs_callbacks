const makeDirWithJSONFilesAndDeleteThoseFiles=require('../problem1');

// passing a to be created directory name
try{
    makeDirWithJSONFilesAndDeleteThoseFiles('myDir',msg=>{
        console.log(msg);
    })
}catch(error){
    console.log(error);
}

/*
// missing to be created directory name
try{
    makeDirWithJSONFilesAndDeleteThoseFiles(msg=>{
        console.log(msg);
    })
}catch(error){
    console.log(error);
}
*/
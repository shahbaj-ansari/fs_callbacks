const fileContentManupulationUsingCallbacks = require('../problem2')


// passing a callback
try{
    fileContentManupulationUsingCallbacks(msg=>{
        console.log(msg);
    });
}catch(error){
    console.log(error);
}

/*

// missing required callback
try{
    fileContentManupulationUsingCallbacks();
}catch(error){
    console.log(error);
}

*/
/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

const fs = require("fs");
const path = require("path");

function fileContentManupulationUsingCallbacks(callback) {
	if (typeof callback != "function") {
		throw new Error("Pass arguments in order : callback:function");
	}

	// 1. Read the given file lipsum.txt
	fs.readFile(path.join(__dirname, "data", "lipsum.txt"), "utf-8", (err, data) => {
		if (err) {
			throw new Error("Error while reading lipsum.txt");
		}

		// 2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
		callback("Read lipsum.txt");
		const dataUpperCase = data.toUpperCase();

		writeFile("lipsumSentencesUpperCase.txt", dataUpperCase, (err, data) => {
			if (err) {
				throw err;
			}

			writeFileName("filenames.txt", "lipsumSentencesUpperCase.txt", (err) => {
				if (err) {
					throw err;
				}

				// 3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
				readFile("lipsumSentencesUpperCase.txt", (err, data) => {
					if (err) {
						throw err;
					}

					const lipsumSentencesLowerCase = data
						.toLocaleLowerCase()
						.split(/[!,?,.]/)
						.join("\n");

					writeFile("lipsumLowercaseSentencesSplitted.txt", lipsumSentencesLowerCase, (err) => {
						if (err) {
							throw err;
						}
						writeFileName("filenames.txt", "lipsumLowercaseSentencesSplitted.txt", (err) => {
							if (err) {
								throw err;
							}
							// 4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
							readFile("filenames.txt", (err, filenm) => {
								if (err) {
									throw err;
								}
								const fileNames = filenm.split("\n");

								for (let i = 0; i < fileNames.length - 1; i++) {
									if (fileNames[i] != "") {
										readFile(fileNames[i], (err, data) => {
											if (err) {
												throw err;
											}
											const sortedData = data.split("\n").sort().toString();

											writeFile(`sorted${fileNames[i]}`, sortedData, (err) => {
												if (err) {
													throw err;
												}
												writeFileName("filenames.txt", `sorted${fileNames[i]}`, (err) => {
													if (err) {
														throw err;
													}
												});
											});
										});
									}
								}

								setTimeout(() => {
									deleteFilesByName("filenames.txt", (err) => {
										if (err) {
											throw err;
										}
									});
								}, 1000);
							});
						});
					});
				});
			});
		});
	});
}

/* Helper Functions */

function writeFile(destination, dataToSave, cb) {
	if (typeof destination != "string" || typeof dataToSave != "string") {
		callback(new Error("writeFile: Put args in correct form"));
	}
	fs.writeFile(path.join(__dirname, "data", destination), dataToSave, (err) => {
		if (err) {
			cb(new Error(`writeFile: Error while writing to ${destination}`));
		} else {
			cb(null);
		}
	});
}

function writeFileName(destination, filenameToSave, cb) {
	if (typeof destination != "string" || typeof filenameToSave != "string") {
		throw new Error("writeFileName: Put args in correct form");
	}
	fs.appendFile(path.join(__dirname, "data", destination), `${filenameToSave}\n`, (err) => {
		if (err) {
			cb(new Error(`writeFileName: Error while writing to ${destination}`));
		} else {
			cb(null);
		}
		console.log(`Stored the name of the new file: ${filenameToSave} in filenames.txt`);
	});
}

function readFile(destination, cb) {
	if (typeof destination != "string" || typeof cb != "function") {
		throw new Error("readFile: Put args in correct form");
	}

	fs.readFile(path.join(__dirname, "data", destination), "utf-8", (err, data) => {
		if (err) {
			cb(new Error(`readFile: Error while reading file: ${destination}`));
		} else {
			cb(null, data);
		}
	});
}

function deleteFilesByName(destination, cb) {
	readFile(destination, (err, data) => {
		if (err) {
			cb(new Error("Error while deleting files"));
		} else {
			const fileNames = data.split("\n");
			for (let i = 0; i < fileNames.length - 1; i++) {
				if (fileNames[i] != "") {
					fs.unlink(path.join(__dirname, "data", fileNames[i]), (err) => {
						if (err) {
							cb(new Error(`Error while deleting file : ${fileNames[i]}`));
						}
						console.log(`${fileNames[i]} : Deleted`);
					});
				}
			}
		}
	});
}

module.exports = fileContentManupulationUsingCallbacks;
